import request from '@/utils/request'
export function getDatasources() {
  return request.get('/maku-generator/gen/table/list')
}
export function getTablePage(params) {
  console.info(params)
  return request.get('/maku-generator/gen/table/page', { params })
}
export function addDatasource(data) {
  return request.post('/maku-generator/gen/table/', data)
}
export function deleteTable(data) {
  return request.delete('/maku-generator/gen/table/', {
    data,
    header: {
      'Content-Type': 'application/json'
    }
  })
}

export function updateData(data) {
  return request.put('/maku-generator/gen/table/',
    data
  )
}
export function testConn(id) {
  return request.get('/maku-generator/gen/table/test/' + id)
}
export function getOneTableInfo(id) {
  return request.get('/maku-generator/gen/table/' + id)
}
export function updateFieldList(id, fieldList) {
  return request.put('/maku-generator/gen/table/field/' + id, fieldList)
}
export function importOneTable(dataSourceId, tableNames) {
  return request.post('/maku-generator/gen/table/import/' + dataSourceId, tableNames)
}
export function asyncTable(tableId) {
  return request.post('/maku-generator/gen/table/sync/' + tableId)
}
