import request from '@/utils/request'
export function getDatasources() {
  return request.get('/maku-generator/gen/datasource/list')
}
export function addDatasource(data) {
  return request.post('/maku-generator/gen/datasource/', data)
}
export function deleteDatasource(data) {
  return request.delete('/maku-generator/gen/datasource/', {
    data,
    header: {
      'Content-Type': 'application/json'
    }
  })
}

export function updateData(data) {
  return request.put('/maku-generator/gen/datasource/',
    data
  )
}
export function testConn(id) {
  return request.get('/maku-generator/gen/datasource/test/' + id)
}
export function importTable(id) {
  return request.get('/maku-generator/gen/datasource/table/list/' + id)
}
