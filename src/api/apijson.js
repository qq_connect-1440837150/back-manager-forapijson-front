import request from '@/utils/request'

export function getData(data) {
  return request.post('/apijson/admin/get', data)
}
export function deleteData(data) {
  return request.post('/apijson/admin/delete', data)
}
export function updateData(data) {
  return request.post('/apijson/admin/put', data)
}
export function addData(data) {
  return request.post('/apijson/admin/post', data)
}
