import { asyncRoutes, constantRoutes } from '@/router'
import { getData } from '@/api/apijson'
import Layout from '@/layout'
import router from '@/router'

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, roles) {
  const res = []

  routes.forEach(route => {
    const tmp = { ...route }

    if (hasPermission(roles, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  })

  return res
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}
// 获取目录
async function getCategory() {
  const result = await getData({
    '[]': {
      'Category': {

      },
      'GenTable': {
        'id@': '[]/Category/table_id'
      }
    }
  })
  console.info(result)

  return result
}
const actions = {
  generateRoutes({ commit }, roles) {
    return new Promise(async resolve => {
      let accessedRoutes
      if (roles.includes('admin')) {
        accessedRoutes = asyncRoutes || []
      } else {
        accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      }
      // commit('SET_ROUTES', accessedRoutes)
      const result = await getCategory()
      const rous = accessedRoutes
      for (const index in result['[]']) {
        // console.info(index)
        const tmp = result['[]'][index]
        console.info(tmp['GenTable']['table_name'])
        rous.push({
          path: '/' + tmp['GenTable']['table_name'],
          component: Layout,
          redirect: '/' + tmp['GenTable']['table_name'],
          children: [
            {
              path: '/' + tmp['GenTable']['table_name'] + '/' + tmp['GenTable']['id'],
              component: () => import('@/views/singleTableTemplate/index'),
              name: tmp['GenTable']['table_name'],
              meta: { title: tmp['GenTable']['table_name'], icon: 'dashboard', affix: false }
            }
          ]
        })
      }
      commit('SET_ROUTES', rous)
      router.addRoutes(rous)
      resolve(accessedRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
