const state = {
  tableId: -2
}
const mutations = {
  CHANGE_TABLE: (state, { key, value }) => {
    // eslint-disable-next-line no-prototype-builtins
    if (state.hasOwnProperty(key)) {
      state[key] = value
    }
  }
}

const actions = {
  changeSetting({ commit }, data) {
    commit('CHANGE_TABLE', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

